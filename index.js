//[SECTION] Comments

/*Comment
-are parts of the code that gets ignored by the language
-meant to describe written code
-Ctrl+Shift+     - Shortcut Key
*/


// [SECTION] Statements and Syntax
// JS statements usually end w/ semicolon (;) but NOT required in JS
	// practice to help us train or locate where statement ends.

// SYNTAX (grammar) set of rules that describes how Statements must be constructed.

console.log("Hello World");

// [SECTION] Variables

// It is used to contain data.

// Declaring a variable

	// Syntax: let/const variableName;
		// let is a keyword that is usually used in declaring a variable

let myVariable;
console.log(myVariable);

	// Initialize value
		// Store the initial data/value of a variable
		// Assignment Operator (=)


myVariable = "Hello";
console.log(myVariable);

myVariable = "Hello World";
console.log(myVariable);

	// const
	// constant(const) variable does not change

// const PI;
// PI = 3.1416;
// console.log(PI) //error: Initial value is missing for const



// Declaring w/ initialization
	//  a variable is given it's initial/starting value upon declaration
		// Syntax let/const variableName = value;

	let productName = "Desktop Computer"
	console.log(productName);

	let productPrice =18999;
	productPrice =20000;

	console.log(productPrice);

	const PI =3.1416;
	console.log(PI);


	/*

	Guide in writing variables.
	1. Use the "let" keyword if the variable will contain different values/can change over time.
		Naming Convention: camelCasing
	2. Use "const" keyword if the variable does not change
		Naming Convention: "const" keyword should be followed by ALL CAPITAL VARIABLE NAME (single value variable).


	3. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion


	4. Variable name are case sensitive.

	*/

// Multiple variable declaration

	let productCode = "DC017", productBrand = "Dell";
	console.log(productCode, productBrand);

// Using a reserve keyword



// [SECTION] Data Types

// In JavaScript, there are six types of data

	// String
		// series of characters that create a word a phrase, sentence or anything related to text.
		// ('') or double quote ("")


	let country ="Philippines";
	let province ='Metro Manila';

	// Concatenating Strings
	// String Values can be combined to create a single String using the "+" symbol.

	let greeting = "I live in " + province + "," +country
	console.log(greeting)

	// Escape Characters 

	// "\n" creates New Line

	let mailAddress = "Quezon City\nPhilippines"
	console.log(mailAddress);

	let message = "John's employees went home early."

	console.log(message)


message = 'Jane\'s employees went home early.'
console.log(message)

	// Numbers
		// Integers/Whole Numbers
	let headcount = 26;
	console.log(headcount);

	// Decimal Numbers
	let decimal = 98.7;
	console.log(decimal);

	// Exponenntial Notation
	let planetDistance = 2e10;
	console.log(planetDistance);

	// Boolean
		// Boolean values are normally used to store values relating to the state of certain things.
		// true or false (logical values)


	let isMarried = false;
	let isGoodConduct = true;

	console.log("isMarried:"+isMarried);
	console.log("isGoodConduct:"+isGoodConduct);

	// Arrays 
		// special kind of data type that can store multiple related values.
		// Syntax: let/Const arrayName = [elementA, elementNth];

	let grades = [98.7, 92.1,90.2,94.6];
	console.log(grades);

		// will work but not recommended

		let details = ["John","Smith", 32, true]
		console.log(details);

	// Objects
		// special kind of data type that is used to mimic real world objects/items
		// used to create complex data that contains information relevant to each other.

		/*
			Syntax:
				let/const objectName = {
					propertyA: valueA,
					propertyB: valueB
				}

		*/

		let person = {
			FullName: "Juan Dela Cruz",
			age:35,
			isMarried: false,
			contact: ["+63912 345 6789", "8123 456"],
			address: {
				houseNumber: "345",
				city: "Manila"
			}

		};

		console.log(person)


		// They're alo useful for creating abstract objects
		let myGrades ={
			firstGrading:98.7,
			secondGrading:92.1,
			thirdGrading:90.2,
			forthGrading:94.6,
		}

		console.log(myGrades);

		// type of operator is used to determine what type of data or the value

		console.log(typeof myGrades); //object

		console.log(typeof grades); //arrays


		[SECTION] NULL

			// absence of value
		let spouse = null;
		console.log(spouse);


		// Undefined
			// indicates that a variable has not been given value yet.
			let FullName
			console.log(fullName);










